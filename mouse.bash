#!/bin/bash
while true
do
    # click on notification
    xdotool mousemove --sync -c 1000 60
    xdotool click 1
    # wiggle around
    sleep 10s
    for i in {1..45}
    do
        sleep 2s
        randomangle=$(( $RANDOM % 360 + 1 ))
        randomdist=$(( $RANDOM % 50 + 10 ))
        xdotool mousemove_relative --sync --polar $randomangle $randomdist
        # Down  0xff54  Move down, down arrow
        xdotool keydown Down sleep 0.1 keyup Down 
    done
    sleep 10s
done
