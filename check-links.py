import requests

filename = "links.txt"

with open(filename) as f:
    urls = f.readlines()

good = []
out = open('myfile.txt', 'w')

for url in urls:
    try:
        url = url.strip()
        print("::"+url+"::")
        response = requests.get(url)

        print(response.status_code)
        if response.status_code == 200:
            out.write("\""+url+"\",\n")
    except:
        pass

out.close()
