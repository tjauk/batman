// ==UserScript==
// @name        Random site
// @namespace   http://tampermonkey.net
// @updateURL   https://bitbucket.org/tjauk/batman/raw/master/random-sites.user.js
// @version     0.2
// @description Open random site from the list
// @author      You
// @match       http*://*/*
// @grant       none
// ==/UserScript==

(function () {
    "use strict";

    var counter = 0;

    function getRandomSite() {
        var sites = [
            "https://www.publish0x.com",
            "https://brave.com/",
            "https://cindicator.com/",
            "https://www.coinbase.com/",
            "https://www.bybit.com/en-US/",
            "https://www.bitpanda.com/en",
            "https://tresorit.com/",
            "https://www.vice.com/",
            "https://www.theguardian.com/international",
        ];
        var randomIndex = Math.floor(Math.random() * sites.length);
        return sites[randomIndex];
    }

    setInterval(function () {
        var url = getRandomSite();
        window.location.href = url;
        counter++;
        console.log(counter, url);
    }, 1000 * 60 * 3);
})();
